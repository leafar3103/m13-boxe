# M13-Boxe

Projeto de site para o Centro de treinamento da M13-Boxe, site one page que tem como objetivo a realização do estudo de HTML e CSS3 junto da hospedagem no repositório, se utilizando do Processo de CI-CD

## O projeto    
A elaboração do projeto é feita pensando em melhorar a comunicação da empresa com os alunos, melhorando o processo de comunicação e melhorando assim a imagem da empresa, além de fornecer aos alunos informações imporntates como horários, apresentação do ambiente, eventos entre outras informações. 

## Processo
Para a elaboração do projeto, foram realizadas as captações e tratamento das imagens, elaboração de assets e design para a implementação no site, elaboração dos conceitos, prototipação do projeto, desde o protótipo de baixa fidelidade através de um Rougth, wireframe, design da interface utilizando o Figma, pensando e procurando otimizar a comunicação de forma simplificada para a elaboração do site. 

